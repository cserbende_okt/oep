# Objektum elvű programozás gyakorlat
## Elérhetőség
  - Cseresnyés Bendegúz
  - cser.bende+okt@gmail.com

## Időpontok
  - Csütörtök 12:00-13:30 2-124
  - Csütörtök 14:00-15:30 00-412
  - Óraszám: 45 perc gyak, 45 perc konzultáció

## Régi Követelmények
  - (2 papíros és) 2 géptermi zárthelyi (1-1 javítási lehetőség)
    - 0, 1, 3, 5 értékelések, 2. ZH-n legalább 3-as elvárt
    - Bármilyen írott vagy nyomtatott segédanyag használható
  - 3 beadandó
    - Max 3 hét késés, hetenként -1 jegy
    - Beadandók átlaga 1 jegyet ér
  - \+ / -  táblás gyakorlaton
  - Szorgalmi feladatok: minden héten, 0.1 jegyet javítanak
  - Végső jegy: 4 ZH és a beadandók összesített jegyének átlaga + szorgalmi feladatok

## Követelmény változások
  - Kötelező házi feladatok lépnek a szorgalmi feladatok helyébe. Ezek házi feladatok egy programtervből és egy programkódból állnak. (Feltöltés: Canvas-be külön-külön, assignment-re egyben zip-ként)
  - 0, 3, 5 osztályzatok kaphatók.
  - A programtervek plusz-mínuszként is funkcionálnak (a 3-as vagy 5-ös terv pluszt ér), így a plusz-mínuszok rendszere tovább él, de emellett a programtervek osztályzatainak átlaga kiválthatja az egyik vagy mindkét papíros zárthelyit (ez későbbi döntéstől függ).
  - A programkódokra kapott jegyek átlaga kiválthatja az egyik vagy mindkét géptermi zárthelyit (ez későbbi döntéstől függ).
  - A szorgalmi feladatok megszűnnek, viszont a már kiadott szorgalmikra megkapható a 0.1 pont

