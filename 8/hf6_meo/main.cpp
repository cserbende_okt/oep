#include <iostream>
#include "enor.h"

using namespace std;

int main()
{
    Enor t("input.txt");
    t.first();
    Result max = t.current();
    
    for(t.next(); !t.end() ; t.next() ){
        if(max.price < t.current().price)
            max = t.current();
    }
    cout << "A legtobb bevetelt a " << max.food << " hozta: " << max.price <<endl;
    return 0;
}
