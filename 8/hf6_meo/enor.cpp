
#include "enor.h"

Enor::Enor(const std::string &str)
{
    _x.open(str);
    if(_x.fail()) throw FILEERROR;
}

void Enor::next()
{
    if ( !(_end = (abnorm==_sx)) ){
        _current.food = _dx.food;
        _current.price = 0;
        for( ; norm==_sx && _dx.food==_current.food; read() ){
            _current.price += _dx.price * _dx.count;
        }
    }
}

void Enor::read()
{
    _x >>_dx.table >> _dx.food >>_dx.time >>_dx.count >> _dx.price;
    _sx = _x.fail() ? abnorm : norm;
}
