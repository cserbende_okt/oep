#pragma once

#include <string>
#include <fstream>

enum Status { abnorm, norm };

struct Order {
    int table;
    std::string food;
    std::string time;
    int count;
    int price;
};

struct Result{
    std::string food;
    int price;
};

class Enor
{
    public:
        enum Errors { FILEERROR };
        Enor(const std::string &str);
        void first() { read(); next(); }
        void next();
        Result current() const { return _current; }
        bool end() const { return _end; }
    private:
        std::ifstream _x;
        Order _dx;
        Status _sx;
        Result _current;
        bool _end;

        void read();
};

