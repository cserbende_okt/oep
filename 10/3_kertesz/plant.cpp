#include "plant.h"

Potatoe *Potatoe::_instance = nullptr;
Potatoe *Potatoe::instance(){
    if(_instance == nullptr){
        _instance = new Potatoe();
    }
    return _instance;
}

Pea *Pea::_instance = nullptr;
Pea *Pea::instance(){
    if(_instance == nullptr){
        _instance = new Pea();
    }
    return _instance;
}