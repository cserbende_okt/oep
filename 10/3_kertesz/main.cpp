#include <iostream>
#include "gardener.h"
#include "plant.h"

using namespace std;

int main()
{
    Garden* garden = new Garden(5);
    Gardener* gardener = new Gardener(garden);

    for(int i = -1; i <= 5;i++){
        try{
            gardener->getGarden()->plant(i, Potatoe::instance(), 3);
            cout << "ultetes sikeres: " << i << endl;
        }
        catch(Garden::Exception e){
            if(e == Garden::Exception::PARCEL_NOT_FOUND)
                cout << "erre a helyre nem lehet ultetni: " << i << endl;
        }
    }

    cout << "A betakarithato parcellak azonositoi: ";
    for( int i : gardener->getGarden()->canHarvest(8)){
        cout << i << " ";
    }
    cout << endl;

    delete garden;
    delete gardener;

    return 0;
}
