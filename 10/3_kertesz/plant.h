#pragma once

/**********************************************/

class Plant
{
    protected:
        int _ripeningTime;
        Plant(int i) : _ripeningTime(i) {}
    public:
        getRipeningTime() const { return _ripeningTime; }
        virtual ~Plant() {}
};

/***********************************************/

class Vegetable : public Plant
{
    protected:
        Vegetable(int i) : Plant(i) {}
};

class Flower : public Plant
{
    protected:
        Flower(int i) : Plant(i) {}
};

/************************************************/

class Potatoe : public Vegetable
{
    private: 
        static Potatoe *_instance;
        Potatoe() : Vegetable(5) { }
    public:
        static Potatoe *instance();
};

class Pea : public Vegetable
{
    private:
        static Pea *_instance;
        Pea() : Vegetable(3) {}
    public:
        static Pea *instance();
};

class Onion : public Vegetable
{
    public:
        Onion() : Vegetable(4) {}
};

class Rose : public Flower
{
    public:
        Rose() : Flower(8) {}
};

class Carnation : public Flower
{
    public:
        Carnation() : Flower(10) {}
};

class Tulip : public Flower
{
    public:
        Tulip() : Flower(7) {}
};
