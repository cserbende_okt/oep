#include "garden.h"

Garden::Garden(int n) {
    _parcels.resize(n);
    for(int i = 0;i < n; i++)
        _parcels[i] = new Parcel(i);
}

Parcel* Garden::getParcel(int i) const {
    if(i < 0 || i >= _parcels.size())
        throw PARCEL_NOT_FOUND;
    return _parcels[i];
}

void Garden::plant(int id, Plant* plant, int date) {
    getParcel(id)->plant(plant, date);
}

std::vector<int> Garden::canHarvest(int date){
    std::vector<int> r;
    for(Parcel *p : _parcels){
        if(p->getPlant() != nullptr && date - p->getplantingDate() == p->getPlant()->getRipeningTime())
            r.push_back(p->getId()); 
    }
    return r;
}