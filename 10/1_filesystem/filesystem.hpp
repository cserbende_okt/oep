#pragma once
#include <vector>

using namespace std;


class Entry{
    public: 
        virtual int getSize() const = 0;
};

class File : public Entry{
    private:
        int size;
    public: 
        File(int s): size(s) {}
        virtual int getSize() const override {
            return size;
        }
};

class Folder : public Entry{
    private:
        vector<Entry *> _entries;
    public: 
        virtual int getSize() const override {
            int s = 0;
            for(Entry *e : _entries){
                s += e->getSize();
            }
            return s;
        }
        void addEntry(Entry *e){
            _entries.push_back(e);
        }
};

class FileSystem : public Folder{};