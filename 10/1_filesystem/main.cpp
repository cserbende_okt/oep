#include <iostream>
#include "filesystem.hpp"

using namespace std;

int main(){
    FileSystem *f = new FileSystem();

    f->addEntry(new File(10));
    f->addEntry(new File(15));

    Folder *d = new Folder();
    f->addEntry(d);
    d->addEntry(new File(13));
    d->addEntry(new File(20));
    

    cout << f->getSize();
}
