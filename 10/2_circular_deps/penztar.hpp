#pragma once

#include <vector>
#include "benzinkut.hpp"

class Benzinkut;

class Penztar {
    private:
        Benzinkut *telephely;
    public:
        Penztar(Benzinkut *b) {
            telephely = b;
        }
        int fizet(int sorszam);
};