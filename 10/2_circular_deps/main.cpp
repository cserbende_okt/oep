#include <iostream>
#include <vector>
#include "benzinkut.hpp"

using namespace std;

int main() {
    const int toltoDb = 6;
    const int penztarDb = 2;

    Benzinkut *_telephely = new Benzinkut(toltoDb,penztarDb);
    _telephely->setEgysegar(280);

    delete _telephely;
    return 0;
}


int Penztar::fizet(int sorszam){
    int liter = telephely->getTolto(sorszam)->getKijelzo();
    telephely->getTolto(sorszam)->lenullaz();
    return liter * telephely->getEgysegar();
}